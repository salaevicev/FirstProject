# FirstProject
### Технологии
Python 3.11
Django 4.2.5
### Запуск проекта в dev-режиме
- Установите и активируйте виртуальное окружение
- Установите зависимости из файла requirements.txt
pip install -r requirements.txt
- В папке с файлом manage.py выполните команду:
python manage.py runserver
